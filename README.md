# First version of the code generator
The main goal of this project is to create a code generator that will generate a CRUD application based on the provided schema. The schema will be provided in the form of a JSON or Yaml file. 
Used template: [react-vite-trpc](https://github.com/kuubson/react-vite-trpc/tree/main)


## ⌨ Scripts

| command                      | description                                                                                     |
| ---------------------------- | ----------------------------------------------------------------------------------------------- |
| `pnpm start`                 | Runs the production build of the server (`/server`)                                             |
| `pnpm pm2:start`             | Runs the server production build as a background process, using pm2 (`/server`)                 |
| `pnpm pm2:delete`            | Deletes all pm2 processes (`/server`)                                                           |
| `pnpm pm2:logs`              | Shows logs for pm2 (`/server`)                                                                  |
| `pnpm dev`                   | Launches apps and bundles all packages in watch mode                                            |
| `pnpm lint`                  | Performs an eslint check through all workspaces                                                 |
| `pnpm lint:fix`              | Performs an eslint fix through all workspaces                                                   |
| `pnpm ts:check`              | Performs a TypeScript check through all workspaces                                              |
| `pnpm ts:references`         | Syncs TypeScript references in all `tsconfig.json` files + updates `nodemon.json` `watch` array |
| `pnpm stylelint`             | Performs an stylelint check through all workspaces                                              |
| `pnpm check`                 | Performs eslint, TypeScript, and stylelint checks through all workspaces                        |
| `pnpm build`                 | Builds all apps                                                                                 |
| `pnpm build:lib`             | Bundles all packages                                                                            |
| `pnpm test:unit`             | Runs unit tests in watch mode                                                                   |
| `pnpm test:unit:run`         | Runs unit tests once                                                                            |
| `pnpm test:integration`      | Runs integration tests in watch mode                                                            |
| `pnpm test:integration:run`  | Runs integration tests once                                                                     |
| `pnpm test:e2e`              | Runs e2e tests in watch mode                                                                    |
| `pnpm test:e2e:run`          | Runs e2e tests once                                                                             |
| `pnpm test:coverage`         | Generates test coverage reports                                                                 |
| `pnpm test:coverage:preview` | Generates test coverage reports and opens preview                                               |
| `pnpm cypress`               | Opens the Cypress UI (`/web`)                                                                   |
| `pnpm cypress:install`       | Installs the Cypress (`/web`)                                                                   |
| `pnpm postinstall`           | Ensures that local or CI environment is ready after installing packages                         |

## 🔒 Envs

Envs are validated with the package `envalid`. Check out `.env-example` & `.env.test-example` files

If the `pnpm dev` script is executed without the required environment variables, the application will output similar details in the console:

```js
================================
Missing environment variables:
PORT: Port the Express server is running on (eg. "3001"). See https://expressjs.com/en/starter/hello-world.html
================================
```

## 🌐 Ports

-  🌐 :3000 - Web
-  🖥️ :3001 - Server

## 📜 License

[The MIT License (MIT)](./LICENSE)
